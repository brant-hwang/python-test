#!/bin/bash

kill `ps -ef | grep manage.py | grep -v grep | grep -v tail | awk '{print $2}'` >/dev/null 2>&1 || true
nohup python3 manage.py runserver 0.0.0.0:8000 >/dev/null 2>&1 &